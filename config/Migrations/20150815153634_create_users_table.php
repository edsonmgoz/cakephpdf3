<?php

use Phinx\Migration\AbstractMigration;

class CreateUsersTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
    public function change()
    {
        $table = $this->table('users');
        $table->addColumn('fullname', 'string', array('limit' => 150))
              ->addColumn('username', 'string', array('limit' => 100))
              ->addColumn('password', 'string')
              ->addColumn('role', 'enum', array('values' => 'admin, user'))
              ->addColumn('created', 'datetime')
              ->addColumn('modified', 'datetime')
              ->create();
    }
}
